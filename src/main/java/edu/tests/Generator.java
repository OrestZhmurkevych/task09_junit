package edu.tests;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;


public class Generator {

    public List<Integer> getIntListByRange(int rangeStart, int rangeEnd, int amount) {
        final IntStream stream = new Random().ints(rangeStart, rangeEnd).limit(amount);
        final List<Integer> intList = stream.boxed().collect(Collectors.toList());
        return intList;
    }

    public List<Integer> getIntListByAmount(int amount) {
        final List<Integer> result = Stream
                .generate(new Random()::nextInt)
                .limit(amount)
                .collect(Collectors.toList());
        return result;
    }
}

package edu.tests;

import java.util.List;

public class ListManager {

    private static final int CONSTANT_FOR_TEST = 5;

    public void displaySumOfElements(final List<Integer> intList) {
        final int sum = getSumOfElements(intList);
        System.out.println("The sum of elements using stream API(sum): " + sum);
    }

    public int getSumOfElements(List<Integer> intList) {
        return intList.stream()
                .mapToInt(Integer::intValue)
                .sum();
    }

    public double getAverageOfElements(final List<Integer> intList) {
        final double average = intList.stream()
                .mapToInt(Integer::intValue)
                .average()
                .getAsDouble();
        return average;
    }

    public void displayAverageOfElements(final List<Integer> intList) {
        final double averageOfElements = getAverageOfElements(intList);
        System.out.println("The average value of elements using stream API(average): " + averageOfElements);
    }

    public void displayMinOfElements(final List<Integer> intList) {
        final int min = getMinOfElements(intList);

        System.out.println("The minimum value of elements using stream API(min): " + min);
    }

    public int getMinOfElements(List<Integer> intList) {
        return intList.stream().mapToInt(Integer::intValue).min().getAsInt();
    }

    public void displayMaxOfElements(final List<Integer> intList) {
        final int max = getMaxOfElements(intList);
        System.out.println("The maximum value of elements using stream API(max): " + max);
    }

    public int getMaxOfElements(List<Integer> intList) {
        return intList.stream().mapToInt(Integer::intValue).max().getAsInt();
    }

    public int getBiggerThanAverageElements(final List<Integer> intList) {
        final int count = Long.valueOf(intList.stream()
                .filter((intValue) -> intValue > getAverageOfElements(intList))
                .count()).intValue();
        return count;
    }

    public double division(int a, int b) {
        if (b == 0) {
            throw new IllegalArgumentException("Do not divide by zero!");
        }
        return a / b;
    }
}

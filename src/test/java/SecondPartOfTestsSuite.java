import edu.tests.ListManagerSecondTest;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.SelectClasses;
import org.junit.platform.suite.api.SuiteDisplayName;
import org.junit.runner.RunWith;

@RunWith(JUnitPlatform.class)
@SuiteDisplayName("JUnit 5 Suite Demo")
@SelectClasses(ListManagerSecondTest.class)
public class SecondPartOfTestsSuite {
}

package edu.tests;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import java.util.Arrays;
import java.util.List;
import static org.mockito.Mockito.spy;


public class ListManagerFirstTest {

    private final static List<Integer> integerList = Arrays.asList(2, 6, 4);
    private ListManager unitToTest = new ListManager();


    @Test
    public void getAverageOfElementsTest() {
        final double actualResult = unitToTest.getAverageOfElements(integerList);
        Assert.assertEquals(4.0, actualResult, 0.0d);
    }

    @Test
    public void displayAverageOfElementsTest() {
        ListManager manager = spy(ListManager.class);

        manager.displayAverageOfElements(integerList);
        Mockito.verify(manager).getAverageOfElements(integerList);
    }

    @Test
    public void getSumOfElementsTest() {
        final int actualResult = unitToTest.getSumOfElements(integerList);
        Assert.assertEquals(12, actualResult);
    }

    @Test
    public void displaySumOfElementsTest() {
        ListManager spyManager = spy(ListManager.class);
        spyManager.displaySumOfElements(integerList);
        Mockito.verify(spyManager).getSumOfElements(integerList);
    }

    @Test
    public void constantTest(){
        try {
            final String actualResult = unitToTest.getClass().getDeclaredField("CONSTANT_FOR_TEST").getType().getSimpleName();
            Assert.assertEquals("int", actualResult);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
    }
}

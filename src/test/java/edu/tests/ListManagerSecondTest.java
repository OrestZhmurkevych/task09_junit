package edu.tests;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.spy;


public class ListManagerSecondTest {

    private final static List<Integer> integerList = Arrays.asList(2, 6, 4);
    private ListManager unitToTest = new ListManager();


    @Test
    public void getMinOfElementsTest() {
        final int actualResult = unitToTest.getMinOfElements(integerList);
        Assert.assertTrue(actualResult == 2);
    }

    @Test
    public void displayMinOfElementsTest() {
        ListManager spyManager = spy(ListManager.class);
        spyManager.displayMinOfElements(integerList);
        Mockito.verify(spyManager).getMinOfElements(integerList);
    }

    @Test
    public void getMaxOfElementsTest() {
        final int actualResult = unitToTest.getMaxOfElements(integerList);
        Assert.assertTrue(actualResult == 6);
    }

    @Test
    public void displayMaxOfElementsTest() {
        final ListManager spyManager = spy(ListManager.class);
        spyManager.displayMaxOfElements(integerList);
        Mockito.verify(spyManager).getMaxOfElements(integerList);
    }

    @Test
    public void divisionTest() {
        final double actualResult = unitToTest.division(6, 3);
        Assert.assertEquals(2.0, actualResult, 0.0d);
    }

    @Test
    public void divisionTestWhenBIsZero() {
        assertThrows(IllegalArgumentException.class, () -> unitToTest.division(10, 0));
    }
}
